const hamburgerToggle = document.querySelector('.hamberger-toggle label input')
const navContent = document.querySelector('.nav-wrapper')

hamburgerToggle.addEventListener('click', function() {
    navContent.classList.toggle("open")
})

// Slider
const slider = document.querySelector('.slider-wrapper')
const bigArticle = document.querySelector('.big-article')
const prevButton = document.querySelector('.slider-control .prev')
const nextButton = document.querySelector('.slider-control .next')
let activeSlide = 1

prevButton.addEventListener('click', function(){
    const bigArticleWidth = bigArticle.clientWidth
    if(activeSlide === 2){
        slider.scrollLeft =- bigArticleWidth
        activeSlide = 1
    } else {
        slider.scrollLeft =+ bigArticleWidth
        activeSlide = 2
    }
})

nextButton.addEventListener('click', function(){
    const bigArticleWidth = bigArticle.clientWidth
    if(activeSlide === 2){
        slider.scrollLeft =- bigArticleWidth
        activeSlide = 1
    } else {
        slider.scrollLeft =+ bigArticleWidth
        activeSlide = 2
    }
})